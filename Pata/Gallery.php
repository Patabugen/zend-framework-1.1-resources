<?php
class Pata_Gallery
{
	protected $parent;
	protected $galleries;
	protected $files;
	protected $filteredFiles = array();
	protected $config;
	
	protected $path;
	protected $subdir;
	protected $baseUrl;
	protected $fileClassName = "Pata_File";
	
	/**
	 * The constructor takes an array of options which can have any of the following items:
	 *	path		- The file system path to the gallery root
	 *	subdir		- If this gallery represents a sub-directory, specify it here
	 *	baseUrl	- The base URL for the gallery (if you want to be able to use getUrl().
	 * It does not matter if you use trailing slashes on paths, they will be stripped.
	 * 	 * @param array $options 
	 */
	public function __construct(array $options = array())
	{
		if (isset($options['path'])) {
			$path = rtrim($options['path'], '\/');
			$this->path = $path;
		}
		
		if (isset($options['subdir'])) {
			$subdir = trim($options['subdir'], '\/');
			$this->subdir = '/'.$subdir;
		} else {
			$this->subdir = '/';
		}

		if (isset($options['baseUrl'])) {
			$baseUrl = rtrim($options['baseUrl'], '\/');
			$this->baseUrl = $baseUrl;
		}
		
		if (isset($options['fileClassName'])) {
			$this->fileClassName = $options['fileClassName'];
		}

	}
	
	public function setSubdir($subdir)
	{
		$this->subdir = $subdir;
	}
	
	public function getSubdir()
	{
		return $this->subdir;
	}
	
	public function getPath()
	{
		return $this->path;
	}
	
	/*
	* Returns the fill path, including subfolder
	*/
	public function getFullPath()
	{
		$path = $this->getPath().'/'.$this->getSubdir();
		$fullPath = realpath($path);
		if ($fullPath == false) {
			throw new Exception("Full path does not exist: $path");
		}
		return $fullPath;
	}

	public function setBaseUrl($path)
	{
		$this->baseUrl = $path;
		$this->_updateChildUrls();
	}

	public function getBaseUrl()
	{
		return $this->baseUrl;
	}

	public function getUrl()
	{
		$subdir = $this->getSubdir() == '' ? '' : $this->getSubdir();
		$parts = explode('/', $subdir);
		$subdir = array();
		foreach ($parts as $part) {
			$subdir[] = urlencode($part);
		}
		$subdir = implode('/', $subdir);
		$path = $this->getBaseUrl().$subdir;
		return $path;
	}

	public function setFiles($files)
	{
		$this->files = $files;
	}

	public function getFiles($filter = null)
	{
		if ($this->files == null) {
			$array = array();
			$files = glob($this->getFullPath().'/*');
			foreach ($files as $file) {
				if (is_file($file)) {
					$relativePath = substr($file, strlen($this->getFullPath()));
					/* @var $file Pata_File */
					$options = array(
						'path'	=> $file,
						'relativePath'	=> $relativePath,
					);
					$file = $this->_getNewFile($options);
					$this->_applyFileUrls($file);
					$file->setDescription($this->getConfig()->getFileDescription($file->getName()));
					$array[] = $file;
				}
			}
			$this->setFiles($array);
		}
		if ($filter != null) {
			if (!isset($this->filteredFiles[$filter])) {
				$extentions = $this->getFileTypeExtentions($filter);
				$filteredFiles = array();
				foreach ($this->files as $file) {
					if (in_array(strtolower($file->getExtention()), $extentions)) {
						$filteredFiles[] = $file;
					}
				}
				$this->filteredFiles[$filter] = $filteredFiles;
			}
			return $this->filteredFiles[$filter];
		}
		return $this->files;
	}
	
	/**
	 * Given a file name or numeric index, returns the given file.
	 * @param type $fileName
	 * @return Pata_File
	 */
	public function getFile($fileNameOrIndex)
	{
		$counter = 0;
		foreach ($this->getFiles() as $file) {
			/* @var $file Pata_File */
			if ($file->getName() == $fileNameOrIndex || $counter === $fileNameOrIndex) {
				$this->_applyFileUrls($file);
				return $file;
			}
			$counter++;
		}
		return false;
	}
	
	public function getFileIndex($filename)
	{
		if ($filename InstanceOf Pata_File) {
			$filename = $filename->getName();
		}
		$counter = 0;
		foreach ($this->getFiles() as $file) {
			/* @var $file Pata_File */
			if ($filename == $file->getName()) {
				return $counter;
			}
			$counter++;
		}
		return false;
	}
	
	public function setGalleries($galleries)
	{
		$this->galleries = $galleries;
	}

	public function deleteFile($file)
	{
		$file = str_replace(array('../', '..\\'), '', $file);
		$file = realpath($this->getFullPath().'/'.$file);
		if (!is_file($file)) {
			throw new Exception("Specified path is not a file");
		}
		unlink($file);
	}

	public function deleteFolder($folder)
	{
		$folder = str_replace(array('../', '..\\'), '', $folder);
		$folder = realpath($this->getFullPath().'/'.$folder);
		if (!is_dir($folder)) {
			throw new Exception("Specified path is not a file");
		}
		
		// Recursive Remove Dir, from php.net comments
		function rrmdir($dir) {
			if (is_dir($dir)) {
				$objects = scandir($dir);
				foreach ($objects as $object) {
					if ($object != "." && $object != "..") {
						if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
					}
				}
				reset($objects);
				rmdir($dir);
			}
		} 
		return rrmdir($folder);
	}
	
	public function createFolder($name)
	{
		$name = str_replace(array('../', '..\\'), '', $name);
		$name = $this->getFullPath().'/'.$name;
		if (file_exists($name)) {
			throw new Exception("Specified folder already exists");
		}
		mkdir($name, 0755, true);
		return realpath($name) !== false;
	}
	
	public function getGalleries()
	{
		if ($this->galleries == null) {
			$array = array();
			$folders = glob($this->getFullPath().'/*');
			foreach ($folders as $folder) {
				if (is_dir($folder)) {
					$folder = substr($folder, strlen($this->getPath()));
//					if (substr($folder, 0, 1) == '_') {
//						continue;
//					}
					$options = array(
						'path'		=> $this->getPath(),
						'subdir'	=> $folder,
						'baseUrl'	=> $this->getBaseUrl(),
						'fileClassName'	=> $this->getFileClassName(),
					);
					$gallery = $this->_getNewGallery($options);
					$array[] = $gallery;
				}
			}
			$this->setGalleries($array);
		}
		return $this->galleries;
	}

	public function getFileClassName()
	{
		return $this->fileClassName;
	}
	
	public function getName()
	{
		$name = basename($this->getPath().'/'.$this->getSubdir());
		return $name;
	}

	/**
	 * Returns a gallery object representing the directory above the current, unless we are at the top in which
	 * case returns false;
	 * @return Pata_Gallery
	 */
	public function getParent()
	{
		if($this->parent === null) {
			if ($this->getSubdir() != '/') {
				$subdir = explode('/', $this->getSubdir());
				array_pop($subdir);
				$subdir = '/'.implode("/", $subdir);
				$options = array(
					'path'		=> $this->getPath(),
					'subdir'	=> $subdir,
					'baseUrl'	=> $this->getBaseUrl(),
					'fileClassName'	=> $this->getFileClassName(),
				);
				$parent = $this->_getNewGallery($options);
				$this->parent = $parent;
			} else {
				$this->parent = false;
			}
		}
		return $this->parent;
	}

	public function getAllParents()
	{
		$allParents = array();
		$newParent = $this->getParent();
		while ($newParent !== false) {
			if ($newParent != false) {
                            array_unshift($allParents, $newParent);
			}
			$newParent = $newParent->getParent();
		}
		return $allParents;
	}
	
	protected function _urlEncode($string)
	{
		$string = urlencode($string);
		return $string;
	}

	protected function _updateChildUrls()
	{
		if ($this->galleries != null) {
			foreach ($this->getGalleries() as $gallery) {
				$gallery->setBaseUrl($this->getBaseUrl());
			}
		}
		if ($this->galleries != null) {
			foreach ($this->getFiles() as $file) {
				$this->_applyFileUrls($file);
			}
		}
	}

	/**
	 * You may override this function to customise the URLs generates
	 * @param type $file 
	 */
	protected function _applyFileUrls($file)
	{
		$file->setUrl($this->getUrl().'/'.$this->_urlEncode($file->getName()));
		$file->setThumbUrl($this->getUrl().'/thumbs/'.$this->_urlEncode($file->getName()));
		$file->setPageUrl($this->getUrl().'/view/'.$this->_urlEncode($file->getName()));
	}
	
	protected function _getNewGallery($options)
	{
		$galleryClassName = get_class($this);
		$gallery = new $galleryClassName($options);
		return $gallery;
	}

	protected function _getNewFile($options){
		$fileClassName = $this->getFileClassName();
		$file = new $fileClassName($options);
		return $file;
	}
	
	/**
	*	Loads and parses the config file, .galleryInfo.json found in the root of the directory.
	**/
	public function getConfig()
	{
		if ($this->config == null) {
			$configPath = $this->getFullPath() . '/.galleryInfo.json';
			$this->config = new Pata_Gallery_Config($configPath);
		}
		return $this->config;
	}
	
	// Saves the config to the root of the directory
	public function saveConfig()
	{
		$files = array();
		foreach ($this->getFiles() as $file) {
			$files[$file->getName()] = $file->getDescription();
		}
		$this->getConfig()->setFiles($files);
		return $this->getConfig()->save();
	}
	
	public function getDescription()
	{
		return $this->getConfig()->getDescription();
	}
	
	public function setDescription($description)
	{
		return $this->getConfig()->setDescription($description);
	}
	
	public function __get($name)
	{
		throw new Exception($name.' is not a valid property');
	}
	
	public function __set($name, $value)
	{
		throw new Exception($name.' is not a valid property');
	}
	
	function getFileTypeExtentions($type = null)
	{
		$types = array(
			'images'		=> array('jpg','jpeg','gif','png','tiff','bmp'),
			'documents'		=> array('doc', 'docx', 'txt', 'rtf', 'odt', 'pdf'),
			'spreadsheets'	=> array('xls'),
		);
		if ($type != null) {
			if (!isset($types[$type])) {
				throw new Exception('Pata_Gallery->getFileTypeExtentions() does not have a list of extentions for the "'.$type.'" type');
			}
			return $types[$type];
		}
		return $types;
	}
}
