<?php
/**
*	Validator for a Zend_Form element which lets you enter dates in human-readable
*	format and this validator will convert them to a real date using strtotime() if possible.
*	For example this lets you enter something like "next thursday 12pm at 4pm" into a
*	form and click Submit. The data will be converted to "2013-12-5 12:00" in time for
*	you to save it, or re-display it. If the date cannot be calculated, and error will be
*	displayed.
*
*	The aim of this validator is for easy entry, it does not attempt to save the entered
*	value after converting it.
*	
*	Limitations:
*		- This works by assuming you're never going to want to enter something which
*			equates to 1st January 1970 00:00
*
*
*	Usage:
*	You need to set the Element using setElement() before you can use it, this is so that
*	the validator can update the value after it's converted it.
*
*		// MyForm.php
*		$startTime = new Zend_Form_Element_Text('startTime');
*		$startTime->setLabel('Start Time');
*		$startTimeEasyTime = new Pata_Validate_EasyTime();
*		$startTimeEasyTime->setElement($startTime);
*		$startTime->addValidator($startTimeEasyTime, false);
*		$form->addElement($startTime);	
*
*		// ProcessMyForm.php
*		$startDate = $form->getElement('startTime')->getValue();
*		// $startDate is in the format of Y-m-d H:i
*	
**/
class Pata_Validate_EasyTime extends Zend_Validate_Abstract {

	const NOT_UNDERSTOOD = 'not_understood';

	protected $_element;
	protected $_messageTemplates = array(
		self::NOT_UNDERSTOOD => "I did not understand that time, if in doubt you can always use yyyy-mm-dd hh:ii"
	);

	public function setElement($element) {
		$this->_element = $element;
	}

	public function isValid($value) {
		$this->_setValue($value);

		if (!isset($this->_element)) {
			throw new Exception(get_class($this).' needs to be given the form element in order to work. Set it using setElement()');
		}
		if ($value == '' && $this->_element->isRequired()) {
			// Empty is a valid if the element is not required
			return true;
		}
		$time = strtotime($value);
		if ($time == 0) {
			$this->_error(self::NOT_UNDERSTOOD);
			return false;
		}

		$this->_element->setValue(date('Y-m-d H:i', $time));

		return true;
	}

}