<?php
/**
 * @author Patabugen
 *	Adapted by Patabugen based on https://gist.github.com/jakebellacera/635416
 *	Used to generate icalendar files (.ics)
 *	
 * Usage:
 *	$icalendar = new Pata_ICalendar();
 *  $icalendar->summary = "Some Summary";
 *	.... set all the properties here
 *	$icalendar->getHttpHeaders() - returns an array of HTTP headers you probably want to set if you're outputting to a browser
*	$icalendar->getContent() - get the content (perhaps to echo it);
*/
// Notes:
// - the UID should be unique to the event, so in this case I'm just using
// uniqid to create a uid, but you could do whatever you'd like.
//
// - iCal requires a date format of "yyyymmddThhiissZ". The "T" and "Z"
// characters are not placeholders, just plain ol' characters. The "T"
// character acts as a delimeter between the date (yyyymmdd) and the time
// (hhiiss), and the "Z" states that the date is in UTC time. Note that if
// you don't want to use UTC time, you must prepend your date-time values
// with a TZID property. See RFC 5545 section 3.3.5
//
// - The Content-Disposition: attachment; header tells the browser to save/open
// the file. The filename param sets the name of the file, so you could set
// it as "my-event-name.ics" or something similar.
//
// - Read up on RFC 5545, the iCalendar specification. There is a lot of helpful
// info in there, such as formatting rules. There are also many more options
// to set, including alarms, invitees, busy status, etc.



// https://www.ietf.org/rfc/rfc5545.txt

class Pata_ICalendar{
	public $summary;		// text title of the event
	public $datestart;	// the starting date (in seconds since unix epoch)
	public $dateend;		// the ending date (in seconds since unix epoch)
	public $address;		// the event's address
	public $uri;			// the URL of the event (add http://)
	public $description;	// text description of the event
	public $filename;	// the name of this file for saving (e.g. my-event-name.ics)

	public $uid;			// A unique number for this event

	public function __construct(){
		$this->uid = uniqid();
		$this->filename = 'event.ics';
	}

	public function getHttpHeaders(){
		$headers = array(
			'Content-type: text/calendar; charset=utf-8',
			'Content-Disposition: attachment; filename=' . $this->filename,
		);
		return $headers;
	}

	// Converts a unix timestamp to an ics-friendly format
	// NOTE: "Z" means that this timestamp is a UTC timestamp. If you need
	// to set a locale, remove the "\Z" and modify DTEND, DTSTAMP and DTSTART
	// with TZID properties (see RFC 5545 section 3.3.5 for info)
	//
	// Also note that we are using "H" instead of "g" because iCalendar's Time format
	// requires 24-hour time (see RFC 5545 section 3.3.12 for info).
	protected function dateToCal($timestamp) {
		$format = 'Ymd\THis\Z';
		if ($timestamp instanceOf DateTime) {
			return $timestamp->format('Ymd\THis\Z');
		}
		return date($format, $timestamp);
	}
	 
	// Escapes a string of characters
	protected function escapeString($string) {
		return preg_replace('/([\,;])/','\\\$1', $string);
	}
	
	public function getContent()
	{
		return <<<ICAL
			BEGIN:VCALENDAR
			VERSION:2.0
			PRODID:-//hacksw/handcal//NONSGML v1.0//EN
			CALSCALE:GREGORIAN
			BEGIN:VEVENT
			DTEND:{$this->dateToCal($this->dateend)}
			UID:{$this->uid}
			DTSTAMP:{$this->dateToCal(time())}
			LOCATION:{$this->escapeString($this->address)}
			DESCRIPTION:{$this->escapeString($this->description)}
			URL;VALUE=URI:{$this->escapeString($this->uri)}
			SUMMARY:{$this->escapeString($this->summary)}
			DTSTART: {$this->dateToCal($this->datestart)}
			END:VEVENT
			END:VCALENDAR
ICAL;
	}
}