<?php

class Pata_Exception_FileUpload extends Zend_Controller_Action_Exception
{
    public function __construct($filename, $code) {
        $message = $this->codeToMessage($code, $filename);
        parent::__construct($message, $this->codeToHttpCode($code));
    }

    /**
    *	Some errors are probably the users fault, others are a server issue.
    **/
    private function codeToHttpCode($code)
    {
    	switch ($code) {
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
            case UPLOAD_ERR_EXTENSION:
            case UPLOAD_ERR_NO_FILE:
            	return 400; // Bad Request
            case UPLOAD_ERR_PARTIAL:
            case UPLOAD_ERR_NO_TMP_DIR:
            case UPLOAD_ERR_CANT_WRITE:
            default:
            	return 500;
    	}
    }
    private function codeToMessage($code, $filename)
    {
        switch ($code) {
            case UPLOAD_ERR_INI_SIZE:
                $message = "The file '{$filename}' is too big, the maximum size is {$this->getMaxUploadSize()} (Error Code: {$code})";
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $message = "The file '{$filename}' is too big, the maximum size is {$this->getMaxUploadSize()} (Error Code: {$code})";
                break;
            case UPLOAD_ERR_PARTIAL:
                $message = "The file '{$filename}' was only partially uploaded (Error Code: {$code})";
                break;
            case UPLOAD_ERR_NO_FILE:
                $message = "No file was uploaded (Error Code: {$code})";
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $message = "Missing a temporary folder (Error Code: {$code})";
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $message = "Failed to write file to disk (Error Code: {$code})";
                break;
            case UPLOAD_ERR_EXTENSION:
                $message = "The file  '{$filename}' has a blocked file extention. (Error Code: {$code})";
                break;
            default:
                $message = "Unknown upload error (Error Code: {$code})";
                break;
        }
        return $message;
    }


    /**
    *		Gets the smallest of several metrics which determin the maximum upload size
    **/
	private function getMaxUploadSize()
	{
		$max_upload = $this->toBytes(ini_get('upload_max_filesize'));
		$max_post = $this->toBytes(ini_get('post_max_size'));
		$memory_limit = $this->toBytes(ini_get('memory_limit'));
		$maxFileSize = min($max_upload, $max_post, $memory_limit);
		return $this->toHumanReadable($maxFileSize);
	}

	private function toBytes($val)
	{
		$val = trim($val);
		$last = strtolower(substr($val, -1));

		if ($last == 'g') {
			$val = $val * 1024 * 1024 * 1024;
		} elseif ($last == 'm') {
			$val = $val * 1024 * 1024;
		} else if ($last == 'k') {
			$val = $val * 1024;
		}
		return $val;
	}

	private function toHumanReadable($bytes)
	{
	    $labels = array('B','KB','MB','GB','TB');
	    for($x = 0; $bytes >= 1024 && $x < (count($labels) - 1); $bytes /= 1024, $x++);
	    return(round($bytes, 2).$labels[$x]);
	}
} 
