<?php
class Pata_Exception_403 extends Zend_Controller_Action_Exception
{

	public function __construct($message = 'Access Denied', $error = 403)
	{
		parent::__construct($message, $error);
	}
}
