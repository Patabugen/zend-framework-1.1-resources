<?php
class Pata_Exception_404 extends Zend_Controller_Action_Exception
{

	public function __construct($message, $error = 404)
	{
		parent::__construct($message, $error);
	}
}
