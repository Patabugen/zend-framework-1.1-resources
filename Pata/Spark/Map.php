<?php

class Pata_Spark_Map
{
	protected $_attributes;
	protected $_fileData;
	protected $_fileName;

	public function __construct($filename)
	{
		$this->_fileName = $filename;
	}

	public function getFileData()
	{
		if($this->_fileData == null){
			if(!file_exists($this->getFilePath()))
				throw new Exception("Map file does not exist: ".$this->getFilePath());
			if(!is_readable($this->getFilePath()))
				throw new Exception("Map file is not readable: ".$this->getFilePath());
			$this->_fileData = file_get_contents($this->getFilePath());
		}
		return $this->_fileData;
	}

	public function getFilePath()
	{
		return $this->_fileName;
	}

	public function getFileName()
	{
		return basename($this->_fileName);
	}

	public function getByteFromData($byte, $length = 1){
		return ord(substr($this->getFileData(), $byte, $length));
	}

	public function getMaxPlayers()
	{
		return $this->getByteFromData(9);
	}

	public function getHoldTime()
	{
		return $this->getByteFromData(10);
	}

	public function getTeams()
	{
		return $this->getByteFromData(11);
	}

	public function getMaxPowerups()
	{
		return $this->getByteFromData(12);
	}

	public function getMissilesEnabled()
	{
		return (bool) $this->getByteFromData(16);
	}

	public function getNadesEnabled()
	{
		return (bool) $this->getByteFromData(17);
	}

	public function getBounciesEnabled()
	{
		return (bool) $this->getByteFromData(18);
	}

	public function getLaserDamage()
	{
		return $this->getByteFromData(13);
	}

	public function getSpecialDamage()
	{
		return $this->getByteFromData(14);
	}

	public function getRechargeRate()
	{
		return $this->getByteFromData(15);
	}

	public function getGameObjective()
	{
		return $this->getByteFromData(12);
	}

	public function getName()
	{
		return $this->getByteFromData(29, intval($this->getByteFromData(27)));
	}

	public function getDescription()
	{
		$nameLength = 29 + strlen($this->getName());
		$nameStart = 31 + strlen($this->getName());
		return $this->getByteFromData($nameStart, $nameLength);
	}
}