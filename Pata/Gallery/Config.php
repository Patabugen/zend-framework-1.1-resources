<?php

class Pata_Gallery_Config
{
	protected $description;
	protected $files = array();
	protected $sourceFile;
	
	public function __construct($source = null)
	{
		// $source can be either an array or a filename
		if ($source != null && !is_array($source) ) {
			$this->sourceFile = $source;
			if( file_exists($source)) {
				$config = file_get_contents($source);
				if ($config == false) {
					throw new Exception('Error loading config file: '.$source);
				}
				$config = json_decode($config, true);
				if ($config == null) {
					throw new Exception('Config file is not valid JSON');
				}
				$source = $config;
			} else {
				$source = array();
			}
		}
		
		if (isset($source['description'])) {
			$this->setDescription($source['description']);
		}
		if (isset($source['files'])) {
			$this->setFiles($source['files']);
		}
	}
	
	public function save($file = null)
	{
		if ($file == null) {
			$file = $this->sourceFile;
		}
		$config = array(
			'description' => $this->getDescription(),
			'files'		=> $this->files
		);
		file_put_contents($file, json_encode($config));
	}
	
	public function setDescription($description)
	{
		$this->description = $description;
	}
	
	public function getDescription()
	{
		return $this->description;
	}
	
	public function getFileDescription($file)
	{
		if (isset($this->files[$file])) {
			return $this->files[$file];
		} else {
			return '';
		}
	}
	
	public function setFiles($files)
	{
		$this->files = $files;
	}
}