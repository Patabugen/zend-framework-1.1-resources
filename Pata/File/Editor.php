<?php

class Pata_File_Editor
{
	protected $file;
	
	public function _construct()
	{
		
	}
	
	public function setFile($file)
	{
		$this->file = $file;
		return $this;
	}
	
	public function getFile()
	{
		return $this->file;
	}
	
	public function rotate($angle)
	{
		require_once('imageTransform.php');
		$transform = new imageTransform();
		$transform->rotate($this->getFile()->getPath(), $angle);
		return true;
	}
	
	public function flip($direction)
	{
		require_once('imageTransform.php');
		$transform = new imageTransform();
		$transform->flipFlop($this->getFile()->getPath(), $direction);
		return true;
	}
	
}