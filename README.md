This is a collection of things I've written for use with Zend Framework 1.

# Installation
Add this to your appliaction.ini:

`autoloaderNamespaces[] = "Pata_"`

and put the files somewhere in your path, for example in the library/ folder of your project.

You should now be able to use Zend's AutoLoader

`$easyDate = new Pata_LValidate_EasyDate`

# Items

See each item itself for a full description (where they've been written). Feel free to get in touch for
more details about anything.

## Pata_Exception_403
## Pata_Exception_404
Exceptions with pre-filled status codes and messages
## Pata_Exception_FileUpload
Exception for throwing when a file upload doesn't work. You pass it the filename and error code and it fills in a message and HTTP status code.

## Pata_File
Works with Pata_Gallery to give you convenient access to filesystems, designed for creating a file
browser especially a photo gallery. Pata_File contains methods for getting thumbnails of icons (either
of the image themselves or file-type icons such as MS Word or Excel).

## Pata_Gallery
## Pata_Gallery_Config
Built alongside Pata_File, Pata_Gallery is for managing a collection of Files. It supports storing meta data
in a .galleryInfo.json via Pata_Gallery_Config such as a Description (for the gallery/folder) and descriptions
for files in the folder.

## Pata_Spark_Map
Simple class for reading Spark Map file data.

```
$map = new Pata_Spark_Map('path/to/mymap.map');
$map->getGameObjective();
```

## Pata_Validate_EasyTime
A Zend validator for allowing you to enter description of time (suitable for strtotime() ) and
have the form calculate the time.